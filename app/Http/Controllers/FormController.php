<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form()
    {
        return view('data.form');        
    }

    public function post(Request $request)
    {
        //dd($request->all());
        $namad = $request -> fname;
        $namab = $request -> lname;
        $gender = $request -> gender;
        $dropdown = $request -> dropdown;
        $langid = $request -> langind;
        $langeng = $request -> langeng;
        $other = $request -> langother;
        $des = $request -> description;
        

        return view('data.index',compact('namad','namab'));
    }
}
